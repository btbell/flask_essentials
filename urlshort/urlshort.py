from flask import render_template, request, redirect, url_for, flash, abort, session, \
    jsonify, Blueprint
import json
import os.path
from werkzeug.utils import secure_filename

bp = Blueprint('urlshort', __name__)

#print(__name__)
@bp.route('/')
def home():
    #return 'Hello Flask!'
    # passing a var
    # return render_template('home.html', name='Fletcher')
    return render_template('home.html', codes=session.keys())

@bp.route('/about')
def about():
    return 'This is a URL shortener created using the Python framework Flask.'

@bp.route('/your-url', methods=['GET', 'POST'])
def your_url():
    if request.method == 'POST':
        urls = {}

        if os.path.exists('urls.json'):
            with open('urls.json') as urls_file:
                urls = json.load(urls_file)

        if request.form['code'] in urls.keys(): #keep in mind 'code' is really the short name entered into the form
            flash('That name already taken. Please type new name.')
            return redirect(url_for('urlshort.home'))

        if 'url' in request.form.keys():
            urls[request.form['code']] = {'url': request.form['url']}
        else:
            fh = request.files['file']
            full_name = request.form['code'] + secure_filename(fh.filename)
            fh.save('/opt/myprojects/url-shortener/urlshort/static/user_files/' + full_name)
            urls[request.form['code']] = {'file': full_name}

        with open('urls.json', 'w') as url_file:
            json.dump(urls, url_file)
            session[request.form['code']] = True
        return render_template('your_url.html', code=request.form['code'])
    else:
        return redirect(url_for('urlshort.home'))

@bp.route('/<string:code>')
def redirect_to_url(code): # remember 'code' refers to "short name"
    if os.path.exists('urls.json'):
        with open('urls.json') as urls_file:
            urls = json.load(urls_file)
            if code in urls.keys():
                if 'url' in urls[code].keys():
                    return redirect(urls[code]['url'])
                else:
                    return redirect(url_for('static', filename='user_files/' + urls[code]['file']))
    return abort(404)

@bp.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404

# API route
@bp.route('/api')
def session_api():
    return jsonify(list(session.keys()))